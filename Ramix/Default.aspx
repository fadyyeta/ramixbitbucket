﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Public.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Ramix.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpTitle" runat="server">
    Homepage 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpHead" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cpContent" runat="server">
    <div class="top_border"></div>
    <div class="rightpart_inner">

        <!-- Hero -->
        <div class="renex_section top" id="home">
            <div class="renex_hero_wrap" data-img-url="img/hero/1.jpg">
                <div class="overlay"></div>
                <div class="hero_texts">
                    <div class="hero_image">
                        <img src="img/about/500x500.jpg" alt="" />
                        <div class="image" data-img-url="img/about/1.jpg"></div>
                    </div>
                    <h3 class="name">Alexander <span class="surname">Bradley</span></h3>
                    <div class="www"><span class="subtitle">I'm passionate <span class="arlo_tm_animation_text_word"></span></span></div>
                </div>
                <div class="renex_down">
                    <div class="line_wrapper">
                        <div class="line"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Hero -->

        <!-- About -->
        <div class="renex_section" id="about">
            <div class="container">
                <div class="renex_title_holder">
                    <span>About Me</span>
                    <h3>Main Informations</h3>
                </div>
                <div class="renex_about">
                    <div class="left">
                        <div class="image_author">
                            <img src="img/about/550x650.jpg" alt="" />
                            <div class="main" data-img-url="img/about/2.jpg"></div>
                        </div>
                    </div>
                    <div class="rightpart">
                        <div class="about_title">
                            <p>Hello! I am <span>Alexander Bradley</span></p>
                        </div>
                        <div class="text">
                            <p>I’m a web developer and graphic designer living in New York, USA. I spend my days with my hands in many different areas of web development from back end programming to front end engineering.</p>
                            <p>I’m extremely passionate about web development and design in all it’s forms and helping small businesses and artisans build and improve their online presence. Aside from web development, I enjoy spending my time with my wife, sons, and daughter, brewing beer and taking photos.</p>
                        </div>
                        <div class="info_list">
                            <ul>
                                <li>
                                    <p><span class="left">Name:</span><span class="right">Alender Bradley</span></p>
                                </li>
                                <li>
                                    <p><span class="left">Birthday:</span><span class="right">22.07.1992</span></p>
                                </li>
                                <li>
                                    <p><span class="left">From:</span><span class="right"><a class="href_location" href="#">22 Brook Street, NYC</a></span></p>
                                </li>
                                <li>
                                    <p><span class="left">Phone:</span><span class="right"><a href="tel:+55 (77) 100 20 20">+55 (77) 100 20 20</a></span></p>
                                </li>
                                <li>
                                    <p><span class="left">Interests:</span><span class="right">Movie, Clip</span></p>
                                </li>
                                <li>
                                    <p><span class="left">Mail:</span><span class="right"><a href="mailto:example@gmail.com">example@gmail.com</a></span></p>
                                </li>
                            </ul>
                        </div>
                        <div class="renex_button">
                            <a href="img/about/cv.jpg" download>Download CV</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /About -->

        <!-- Portfolio -->
        <div class="renex_section" id="products">
            <div class="renex_portfolio">
                <div class="container">
                    <div class="renex_title_holder">
                        <span>Portoflio</span>
                        <h3>Awesome Works</h3>
                    </div>
                    <div class="portfolio_list renex_appear">
                        <ul class="gallery_zoom">
                            <li>
                                <div class="inner">
                                    <div class="image_wrap">
                                        <div class="image">
                                            <img src="img/portfolio/600x850.jpg" alt="" />
                                            <div class="main" data-img-url="img/portfolio/1.jpg"></div>
                                        </div>
                                    </div>
                                    <div class="title">
                                        <h3>Poster Mockup</h3>
                                    </div>
                                    <a class="link zoom" href="img/portfolio/1.jpg"></a>
                                </div>
                            </li>
                            <li>
                                <div class="inner">
                                    <div class="image_wrap">
                                        <div class="image">
                                            <img src="img/portfolio/600x850.jpg" alt="" />
                                            <div class="main" data-img-url="img/portfolio/2.jpg"></div>
                                        </div>
                                    </div>
                                    <div class="title">
                                        <h3>Interesting Book</h3>
                                    </div>
                                    <a class="link zoom" href="img/portfolio/2.jpg"></a>
                                </div>
                            </li>
                            <li>
                                <div class="inner">
                                    <div class="image_wrap">
                                        <div class="image">
                                            <img src="img/portfolio/600x850.jpg" alt="" />
                                            <div class="main" data-img-url="img/portfolio/3.jpg"></div>
                                        </div>
                                    </div>
                                    <div class="title">
                                        <h3>Black Bag</h3>
                                    </div>
                                    <a class="link zoom" href="img/portfolio/3.jpg"></a>
                                </div>
                            </li>
                            <li>
                                <div class="inner">
                                    <div class="image_wrap">
                                        <div class="image">
                                            <img src="img/portfolio/600x850.jpg" alt="" />
                                            <div class="main" data-img-url="img/portfolio/4.jpg"></div>
                                        </div>
                                    </div>
                                    <div class="title">
                                        <h3>Newspaper</h3>
                                    </div>
                                    <a class="link zoom" href="img/portfolio/4.jpg"></a>
                                </div>
                            </li>
                            <li>
                                <div class="inner">
                                    <div class="image_wrap">
                                        <div class="image">
                                            <img src="img/portfolio/600x850.jpg" alt="" />
                                            <div class="main" data-img-url="img/portfolio/5.jpg"></div>
                                        </div>
                                    </div>
                                    <div class="title">
                                        <h3>Yellow Cup</h3>
                                    </div>
                                    <a class="link zoom" href="img/portfolio/5.jpg"></a>
                                </div>
                            </li>
                            <li>
                                <div class="inner">
                                    <div class="image_wrap">
                                        <div class="image">
                                            <img src="img/portfolio/600x850.jpg" alt="" />
                                            <div class="main" data-img-url="img/portfolio/6.jpg"></div>
                                        </div>
                                    </div>
                                    <div class="title">
                                        <h3>Mockup Tools</h3>
                                    </div>
                                    <a class="link zoom" href="img/portfolio/6.jpg"></a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Portfolio -->

        <!-- Contact -->
        <div class="renex_section" id="contact">
            <div class="renex_contact">
                <div class="container">
                    <div class="renex_title_holder">
                        <span>Contact</span>
                        <h3>Get in Touch</h3>
                    </div>
                    <div class="contact_inner">
                        <div class="left">
                            <ul>
                                <li>
                                    <div class="inner">
                                        <img class="svg" src="img/svg/placeholder.svg" alt="" />
                                        <span><a class="href_location" href="#">22 Brook Street, NYC</a></span>
                                    </div>
                                </li>
                                <li>
                                    <div class="inner">
                                        <img class="svg" src="img/svg/telephone.svg" alt="" />
                                        <span><a href="tel:+55 (77) 100 20 20">+55 (77) 100 20 20</a></span>
                                    </div>
                                </li>
                                <li>
                                    <div class="inner">
                                        <img class="svg" src="img/svg/envelope.svg" alt="" />
                                        <span><a href="mailto:yourmail@gmail.com">yourmail@gmail.com</a></span>
                                    </div>
                                </li>
                                <li>
                                    <div class="inner">
                                        <img class="svg" src="img/svg/telegram.svg" alt="" />
                                        <span><a href="https://t.me/@telegramNickName" target="_blank">@telegramNickName</a></span>
                                    </div>
                                </li>
                                <li>
                                    <div class="inner">
                                        <img class="svg" src="img/svg/whatsapp.svg" alt="" />
                                        <span><a href="https://wa.me/@whatsappNickName" target="_blank">@whatsappNickName</a></span>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="right">
                            <form action="http://integrothemes.com/" method="post" class="contact_form" id="contact_form">
                                <div class="returnmessage" data-success="Your message has been received, We will contact you soon."></div>
                                <div class="first_row">
                                    <ul>
                                        <li>
                                            <input id="name" type="text" placeholder="Your Name" />
                                        </li>
                                        <li>
                                            <input id="email" type="text" placeholder="Your Email" />
                                        </li>
                                    </ul>
                                </div>
                                <div class="second_row">
                                    <textarea id="message" placeholder="Your Message"></textarea>
                                </div>
                                <div class="renex_button">
                                    <a id="send_message" href="#">Submit</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Contact -->

    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
