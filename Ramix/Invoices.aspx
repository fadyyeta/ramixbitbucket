﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="Invoices.aspx.cs" Inherits="Ramix.Invoices" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpTitle" runat="server">
    Invoices
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpHead" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cpContent" runat="server">
    <div class="col-md-12">
        <div class="card ">
            <div class="card-header card-header-rose card-header-text">
                <div class="card-text">
                    <h4 class="card-title">Invoice</h4>
                </div>
            </div>
            <div class="card-body ">
                <div class="myForm form-horizontal">
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Invoice Select</label>
                        <div class="col-sm-10">
                            <div class="dropdown bootstrap-select show-tick dropup">
                                <select class="selectpicker" data-style="select-with-transition" title="Choose Invoice" data-size="7" tabindex="-98">
                                    <option disabled="">Please Choose</option>
                                    <option value="2">1005 - mohamed atef pharmacy</option>
                                    <option value="3">15632 - dr ihab pharmacy</option>
                                </select>

                                <div class="dropdown-menu" role="combobox" style="max-height: 276px; overflow: hidden; min-width: 220px; position: absolute; top: 3px; left: 1px; will-change: top, left; width: 100%;" x-placement="top-start">
                                    <div class="inner show" role="listbox" aria-expanded="false" tabindex="-1" style="max-height: 266px; overflow-y: auto;">
                                        <ul class="dropdown-menu inner show">
                                            <li class="disabled"><a role="option" class="dropdown-item disabled" aria-disabled="true" tabindex="-1" aria-selected="false"><span class=" bs-ok-default check-mark"></span><span class="text">Please Choose</span></a></li>
                                            <li><a role="option" class="dropdown-item" aria-disabled="false" tabindex="0" aria-selected="false"><span class=" bs-ok-default check-mark"></span><span class="text">1005 - mohamed atef pharmacy</span></a></li>
                                            <li><a role="option" class="dropdown-item" aria-disabled="false" tabindex="0" aria-selected="false"><span class=" bs-ok-default check-mark"></span><span class="text">15632 - dr ihab pharmacy</span></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Amount</label>
                        <div class="col-sm-10">
                            <div class="form-group bmd-form-group">
                                <input type="text" class="form-control" id="paymentAmount" placeholder="Payment Amount">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Payment Date</label>
                        <div class="col-sm-10">
                            <div class="form-group bmd-form-group">
                                <input type="text" class="form-control datetimepicker" id="paymentDate" value="11/06/2019">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer ">
                <button type="submit" class="btn btn-fill btn-rose">
                    Submit<div class="ripple-container">
                        <div class="ripple-decorator ripple-on ripple-out" style="left: 84px; top: 22.6px; background-color: rgb(255, 255, 255); transform: scale(12.8891);"></div>
                    </div>
                </button>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cpScript" runat="server">
    <script src="assets/js/plugins/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript">
        $(function () {
            $('.datetimepicker').datetimepicker();
        });
    </script>
</asp:Content>
