﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="Sales.aspx.cs" Inherits="Ramix.Sales" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpTitle" runat="server">
    Sales
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpHead" runat="server">
    <style>
        .myForm.form-horizontal {
            margin: 0 20px;
        }

        .myForm label.col-form-label {
            text-align: left !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cpContent" runat="server">
    <div class="col-md-12">
        <div class="card ">
            <div class="card-header card-header-rose card-header-text">
                <div class="card-text">
                    <h4 class="card-title">Sales Report</h4>
                </div>
            </div>
            <div class="card-body ">
                <div class="myForm form-horizontal">

                    <div class="row">
                        <label class="col-sm-2 col-form-label">Pharmacy Name</label>
                        <div class="col-sm-10">
                            <div class="form-group bmd-form-group">
                                <asp:TextBox runat="server" ID="pharmName" CssClass="form-control" placeholder="Pharmacy Name" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Site</label>
                        <div class="col-sm-10">
                            <div class="form-group bmd-form-group">
                                <asp:TextBox runat="server" ID="pharmSite" CssClass="form-control" placeholder="Site" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Area</label>
                        <div class="col-sm-10">
                            <div class="form-group bmd-form-group">
                                <asp:TextBox runat="server" ID="pharmArea" CssClass="form-control" placeholder="Area" />
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <label class="col-sm-2 col-form-label">Number of units sold</label>
                        <div class="col-sm-10">
                            <div class="form-group bmd-form-group">
                                <asp:TextBox runat="server" ID="numSold" CssClass="form-control" placeholder="Number of units sold" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Package price</label>
                        <div class="col-sm-10">
                            <div class="form-group bmd-form-group">
                                <asp:TextBox runat="server" ID="pkgPrice" CssClass="form-control" placeholder="Package price" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Discount</label>
                        <div class="col-sm-10">
                            <div class="form-group bmd-form-group">
                                <asp:TextBox runat="server" ID="Discount" CssClass="form-control" placeholder="Discount" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Bonus</label>
                        <div class="col-sm-10">
                            <div class="form-group bmd-form-group">
                                <asp:TextBox runat="server" ID="bonus" CssClass="form-control" placeholder="Bonus" />
                            </div>
                        </div>
                    </div>
                    <div class="row ohHide">
                        <label class="col-sm-2 col-form-label">Sample</label>
                        <div class="col-sm-10">
                            <div class="form-group bmd-form-group">
                                <asp:TextBox runat="server" ID="Sample" CssClass="form-control" placeholder="Sample" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Bill Number</label>
                        <div class="col-sm-10">
                            <div class="form-group bmd-form-group">
                                <asp:TextBox runat="server" ID="billNum" CssClass="form-control" placeholder="Bill Number" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Product</label>
                        <div class="col-sm-10">
                            <div class="form-group bmd-form-group">
                                <asp:TextBox runat="server" ID="product" CssClass="form-control" placeholder="Product" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label label-checkbox">Payment</label>
                        <div class="col-sm-10 checkbox-radios">
                            <%--<div class="form-check bmd-form-group">
                                form-check form-check-inline
                                <label class="form-check-label">
                                    <input class="form-check-input" name="exampleRadios" type="radio" value="Yes" checked>Yes<span class="circle"><span class="check"></span></span>
                                </label>
                            </div>
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input class="form-check-input" name="exampleRadios" type="radio" value="No">No<span class="circle"><span class="check"></span></span>
                                </label>
                            </div>--%>
                            <div class="form-group bmd-form-group">
                                <asp:TextBox runat="server" ID="payment" CssClass="form-control" placeholder="paid amount" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer ">
                <asp:Button Text="Submit" ID="btnSubmit" ClientIDMode="Static" OnClick="btnSubmit_ServerClick" CssClass="btn btn-fill btn-rose" runat="server" />
                <%--<button type="submit" runat="server" onserverclick="btnSubmit_ServerClick" id="btnSubmit" class="btn btn-fill btn-rose">
                    Submit<div class="ripple-container">
                        <div class="ripple-decorator ripple-on ripple-out" style="left: 84px; top: 22.6px; background-color: rgb(255, 255, 255); transform: scale(12.8891);"></div>
                    </div>
                </button>--%>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
