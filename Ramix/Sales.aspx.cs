﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Ramix
{
    public partial class Sales : System.Web.UI.Page
    {
        CodeClass codeClass = new CodeClass();
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }
        public void TraverseControlsAndSetTextEmpty(Control control)
        {
            foreach (Control c in control.Controls)
            {
                var box = c as TextBox;
                if (box != null)
                {
                    box.Text = string.Empty;
                }

                this.TraverseControlsAndSetTextEmpty(c);
            }
        }
        protected void btnSubmit_ServerClick(object sender, EventArgs e)
        {
            string Pharmacy = pharmName.Text;
            string Site = pharmSite.Text;
            string Area = pharmArea.Text;
            int Units = int.Parse(numSold.Text);
            decimal Price = decimal.Parse(pkgPrice.Text);
            decimal discount = decimal.Parse(Discount.Text);
            decimal Bonus = decimal.Parse(bonus.Text);
            string BillNum = billNum.Text;
            string Product = product.Text;
            decimal PaidAmount = decimal.Parse(payment.Text);
            string InvoiceName = BillNum + " - " + Pharmacy;
            string dateOrder = DateTime.Now.ToShortDateString();
            decimal total = Units * (Price * ((100 - discount) / 100));
            string param = "N'" + InvoiceName + "', N'" + Pharmacy + "', N'" + Site + "', N'" + Area + "', " + Units + ", " + Price + ", " + discount + ", " + Bonus + ", N'" + BillNum + "', 1, " + PaidAmount + ", N'" + dateOrder + "', " + total + ", 1";
            string cols = "invoiceName ,pharmacyName ,site ,area ,numberOfUnitsSold ,packagePrice ,discount ,bonus ,billNumber ,productID ,paid ,dateOrder ,total ,userID ";
            int res = codeClass.inserGen("Invoice", cols, param);
            TraverseControlsAndSetTextEmpty(this);
        }
    }
}