﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Ramix
{
    public partial class Test : System.Web.UI.Page
    {
        CodeClass codeClass = new CodeClass();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                loadInvoice();
            }
        }
        protected void loadInvoice()
        {
            DataSet ds = codeClass.SQLREAD("SELECT * FROM Invoice");
            gvTest.DataSource = ds;
            gvTest.DataBind();
        }
    }

}