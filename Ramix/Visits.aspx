﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="Visits.aspx.cs" Inherits="Ramix.Visits" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpTitle" runat="server">
    Visits
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpHead" runat="server">
    <style>
        .myForm.form-horizontal {
            margin: 0 20px;
        }

        .myForm label.col-form-label {
            text-align: left !important;
        }

        .dropdown.bootstrap-select {
            width: 100% !important;
        }

        .form-group.bmd-form-group.txtArea {
            margin: 5px 15px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cpContent" runat="server">
    <div class="col-md-12">
        <div class="card ">
            <div class="card-header card-header-rose card-header-text">
                <div class="card-text">
                    <h4 class="card-title">Doctor's Feedback</h4>
                </div>
            </div>
            <div class="card-body ">
                <div class="myForm form-horizontal">
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Shift Select</label>
                        <div class="col-sm-10">
                            <div class="dropdown bootstrap-select show-tick dropup">
                                <select class="selectpicker" data-style="select-with-transition" title="Choose Shift" data-size="7" tabindex="-98">
                                    <option disabled="">Please Choose</option>
                                    <option value="2">Morning</option>
                                    <option value="3">Night</option>
                                </select>

                                <div class="dropdown-menu" role="combobox" style="max-height: 276px; overflow: hidden; min-width: 220px; position: absolute; top: 3px; left: 1px; will-change: top, left; width: 100%;" x-placement="top-start">
                                    <div class="inner show" role="listbox" aria-expanded="false" tabindex="-1" style="max-height: 266px; overflow-y: auto;">
                                        <ul class="dropdown-menu inner show">
                                            <li class="disabled"><a role="option" class="dropdown-item disabled" aria-disabled="true" tabindex="-1" aria-selected="false"><span class=" bs-ok-default check-mark"></span><span class="text">Please Choose</span></a></li>
                                            <li><a role="option" class="dropdown-item" aria-disabled="false" tabindex="0" aria-selected="false"><span class=" bs-ok-default check-mark"></span><span class="text">Morning</span></a></li>
                                            <li><a role="option" class="dropdown-item" aria-disabled="false" tabindex="0" aria-selected="false"><span class=" bs-ok-default check-mark"></span><span class="text">Night</span></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Doctor's Name</label>
                        <div class="col-sm-10">
                            <div class="form-group bmd-form-group">
                                <input type="text" class="form-control" id="docName" placeholder="Doctor's Name">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Specialization</label>
                        <div class="col-sm-10">
                            <div class="form-group bmd-form-group">
                                <input type="text" class="form-control" id="docSpec" placeholder="Specialization">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Category</label>
                        <div class="col-sm-10">
                            <div class="form-group bmd-form-group">
                                <input type="text" class="form-control" id="docCat" placeholder="Category">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group" style="width: 100% !important;">
                            <label>Doctor Comments</label>
                            <div class="form-group bmd-form-group txtArea">
                                <label class="bmd-label-floating">Comments about doctor from Rep.</label>
                                <textarea class="form-control" id="repComment" rows="5"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Pharmacies</label>
                        <div class="col-sm-10">
                            <div class="form-group bmd-form-group">
                                <input type="text" class="form-control" id="pharmacies" placeholder="Pharmacies">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group" style="width: 100% !important;">
                            <label>Pharmacy's Comments</label>
                            <div class="form-group bmd-form-group txtArea">
                                <label class="bmd-label-floating">Comments about doctor from Pharmacies</label>
                                <textarea class="form-control" id="pharmComment" rows="5"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Address</label>
                        <div class="col-sm-10">
                            <div class="form-group bmd-form-group">
                                <input type="text" class="form-control" id="address" placeholder="Address">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer ">
                <button type="submit" class="btn btn-fill btn-rose">
                    Submit<div class="ripple-container">
                        <div class="ripple-decorator ripple-on ripple-out" style="left: 84px; top: 22.6px; background-color: rgb(255, 255, 255); transform: scale(12.8891);"></div>
                    </div>
                </button>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cpScript" runat="server">
</asp:Content>
